$(".weapon").click(function(){
    $(".weapon").each(function() {
        $(this).attr("src", $(this).attr("src").replace("2.png", ".png"));
    });

    $(".your-w img").attr("src", $(this).attr("src"));
    $(this).attr("src", $(this).attr("src").replace(".png", "2.png"));
    $(".comp-w img").attr("src", "qm.png");
});
$(".weapon").hover(function(){
    $(".weapon").each(function() {
        $(this).attr("src", $(this).attr("src").replace("2.png", ".png"));
    });
    $(this).attr("src", $(this).attr("src").replace(".png", "2.png"));
    
});

$("#togglerules").click(function(){
  $(".rules").slideToggle(1000);
});

function randomNumber(from,to) {
  return Math.random()*(to-from)+from;
}

let counterLeft = 0;
let counterRight = 0;

$(".weapon").click(function(){
    let images = ["assets/images/rock.png", "assets/images/paper.png", "assets/images/scissors.png", "assets/images/lizard.png", "assets/images/spock.png"]
    let randomImage = images[Math.floor(randomNumber(0, images.length))];
    
    $(".comp-w img").attr("src", randomImage);
    
        if ( ($(".your-w img").attr("src") == "assets/images/scissors.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/lizard.png")   
            || ($(".your-w img").attr("src") == "assets/images/lizard.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/spock.png")
            || ($(".your-w img").attr("src") == "assets/images/spock.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/rock.png")
            || ($(".your-w img").attr("src") == "assets/images/paper.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/rock.png")
            || ($(".your-w img").attr("src") == "assets/images/scissors.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/paper.png")
            || ($(".your-w img").attr("src") == "assets/images/rock.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/scissors.png")
            || ($(".your-w img").attr("src") == "assets/images/spock.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/scissors.png")
            || ($(".your-w img").attr("src") == "assets/images/paper.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/spock.png")
            || ($(".your-w img").attr("src") == "assets/images/lizard.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/paper.png")
            || ($(".your-w img").attr("src") == "assets/images/rock.png" 
            &&  $(".comp-w img").attr("src") == "assets/images/lizard.png")) {
                counterLeft++;
                $(".won").text("You win!");       
        } else if (($(".your-w img").attr("src") == "assets/images/rock.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/paper.png")
                        || ($(".your-w img").attr("src") == "assets/images/paper.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/scissors.png")
                        || ($(".your-w img").attr("src") == "assets/images/lizard.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/scissors.png")
                        || ($(".your-w img").attr("src") == "assets/images/spock.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/lizard.png")
                        || ($(".your-w img").attr("src") == "assets/images/rock.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/spock.png")
                        || ($(".your-w img").attr("src") == "assets/images/scissors.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/spock.png")
                        || ($(".your-w img").attr("src") == "assets/images/spock.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/paper.png")
                        || ($(".your-w img").attr("src") == "assets/images/paper.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/lizard.png")
                        || ($(".your-w img").attr("src") == "assets/images/lizard.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/rock.png")
                        || ($(".your-w img").attr("src") == "assets/images/scissors.png" 
                        &&  $(".comp-w img").attr("src") == "assets/images/rock.png")) {
            counterRight++;
            $(".won").text("You lost!");
        } else {
            $(".won").text("Draw!");
        }
    
    $(".score").text(counterLeft + ":" + counterRight); 
    $(".won").css("padding", ".5rem");
    $("p").css("padding", "0 1rem 1rem");
    

    if(counterLeft >= 3 && counterRight <=2){
        $(".comment").text("Keep it Going!");
    }
    else if(counterLeft >= 5 && counterRight <=3){
        $(".comment").text("You're getting good at this!");
    }
    else if(counterLeft >= 7 && counterRight <=4){
        $(".comment").text("Bazing! You're on fire!");
    }
    else if(counterLeft <= 2 && counterRight >=5){
        $(".comment").text("Its ok, go get'em");
    }
    else if(counterLeft <= 3 && counterRight >=7){
        $(".comment").text("Do you even know how to play this game?");
    }
    else if(counterLeft <= 5 && counterRight >=10){
        $(".comment").text("Waaah!");
    }

});